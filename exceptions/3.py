#Write a function that asks for an integer and prints the square of it. Use a while loop with a try,except, else block to account for incorrect inputs.

def ask():
	while True:
		try:
			n=input("Input an integer:")
		except:
			print("An error occured")
			continue
		else:
			break

	print(int(n)**2)

ask()
