# -*- coding: utf-8 -*-
"""
Created on Sun Aug 20 18:32:32 2017

@author: rishabh
"""

#RANDOM_FOREST_REGRESSION
import numpy as np
import pandas as pd 
import matplotlib.pyplot as  plt

#Importing the dataset
dataset=pd.read_csv('Position_Salaries.csv')
X=dataset.iloc[:,1:2].values
Y=dataset.iloc[:,2].values

#Fitting the random forest regression to the dataset
from sklearn.ensemble import RandomForestRegressor
regressor=RandomForestRegressor(n_estimators=300,random_state=0)
regressor.fit(X,Y)

#Predicting the new result
Y_predict=regressor.predict(6.5)

#Visualizing the regression result (for higher resolution and smoother curve)
X_grid=np.arange(min(X),max(X),0.01)
X_grid=X_grid.reshape(len(X_grid),1)
plt.scatter(X,Y,color='red')
plt.plot(X_grid,regressor.predict(X_grid),color='blue' )
plt.title('Truth or Bluff (Random Forest Regression)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()

