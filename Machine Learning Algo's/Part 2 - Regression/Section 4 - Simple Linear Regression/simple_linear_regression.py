# -*- coding: utf-8 -*-
"""
Created on Sat Aug 19 15:19:17 2017

@author: rishabh
"""

#importing the libraries
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt

#Importing the dataset
dataset=pd.read_csv('Salary_Data.csv') 
X=dataset.iloc[:,:-1].values
Y=dataset.iloc[:,1].values

#splitting the dataset into training set and test set
from sklearn.cross_validation import train_test_split
X_train,X_test,Y_train,Y_test=train_test_split(X,Y,test_size=1/3,random_state=0)

#fitting the simple linear regression to the training set
from sklearn.linear_model import LinearRegression
regressor=LinearRegression()
regressor.fit(X_train,Y_train)

#Fitting Simple Linear Regression to the Training set 
from sklearn.linear_model import LinearRegression
regressor=LinearRegression()
regressor.fit(X_train,Y_train)

Y_pred=regressor.predict(X_test)

#Visualizing the Traininig set results
plt.scatter(X_train,Y_train,color='red')
plt.plot(X_train,regressor.predict(X_train),color='blue')
plt.title('Salary vs Experience (Training set)')
plt.xlabel('Years of experience')
plt.ylabel('Salary')
plt.show()

#Visualizing the Test set results
plt.scatter(X_test,Y_test,color='red')
plt.plot(X_train,regressor.predict(X_train),color='blue')
plt.title('Salary vs Experience (Test set)')
plt.xlabel('Years of experience')
plt.ylabel('Salary')
plt.show()
