#SUPPORT VECTOR REGRESSION
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 20 15:16:07 2017

@author: rishabh
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#importing the dataset
dataset=pd.read_csv('Position_Salaries.csv')
X=dataset.iloc[:,1:2].values
Y=dataset.iloc[:,2:].values


#Feature Scaling 
from sklearn.preprocessing import StandardScaler
sc_X=StandardScaler()
sc_Y=StandardScaler()
X=sc_X.fit_transform(X)
Y=sc_Y.fit_transform(Y)

#Fitting SVR to dataset
from sklearn.svm import SVR 
regressor =SVR(kernel='rbf')
regressor.fit(X,Y)

#Predicting the new result
Y_pred=sc_Y.inverse_transform(regressor.predict(sc_X.transform(np.array([[6.5]]))))

#Visualizing the SVR result
plt.scatter(X,Y,color='red')
plt.plot(X,regressor.predict(X),color='blue')
plt.xlabel('Truth of bluff(svr)')
plt.ylabel('Position Level')
plt.show()

