#POLYNOMIAL LINEAR REGRESSION 
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 20 11:22:24 2017

@author: rishabh
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

dataset=pd.read_csv('Position_Salaries.csv')
X=dataset.iloc[:,1:2].values
Y=dataset.iloc[:,2].values

#Fitting Linear Regression to the dataset 
from sklearn.linear_model import LinearRegression
lin_reg=LinearRegression()
lin_reg.fit(X,Y)


#Fitting Linear Regression to the dataset
from sklearn.preprocessing import PolynomialFeatures
poly_reg=PolynomialFeatures(degree=4)
X_poly=poly_reg.fit_transform(X)
#poly_reg.fit(X_poly,Y)
lin_reg_2=LinearRegression()
lin_reg_2.fit(X_poly,Y)

#Visualizing the linear Regression Results
plt.scatter(X,Y,color='red')
plt.plot(X,lin_reg.predict(X),color='blue')
plt.title('Truth or Bluff(Linear Regression)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()

#Visualising the Polynomial Regression results
plt.scatter(X,Y,color='red')
plt.plot(X,lin_reg_2.predict(poly_reg.fit_transform(X)),color='blue')  
plt.title('Truth or Bluff(Linear Regression)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()

#Predicting the new results with Linear Regression
lin_reg.predict(6.5)

#Predicting the new results with Polynomial Regression
lin_reg_2.predict(poly_reg.fit_transform(6.5))

