# -*- coding: utf-8 -*-
"""
Created on Sat Aug 19 12:27:26 2017

@author: rishabh
"""

#Importing the libraries
import numpy as np
import matplotlib.pyplot as plt 
import pandas as pd

#Importing the datasets
dataset=pd.read_csv('Data.csv')
X=dataset.iloc[:,:-1].values
Y=dataset.iloc[:,3].values

#splitting the dataset into the training set and test set
#tesr_size=0.2 means 20% is the test set and random_state=0 means so that we get the same 
#output as author
from sklearn.cross_validation import train_test_split
X_train,X_test,Y_train,Y_test=train_test_split(X,Y,test_size=0.2,random_state=0)

#feature Scaling 
"""from sklearn.preprocessing import StandardScaler
sc_X=StandardScaler()
X_train=sc_X.fit_transform(X_train)
X_test=sc_X.transform(X_test)
"""