# -*- coding: utf-8 -*-
"""
Created on Fri Sep  8 11:17:31 2017

@author: rishabh
"""

#ANN
#importing the libraries
import numpy as np
import pandas as pd

#importing the dataset
dataset=pd.read_csv('Churn_Modelling.csv')
X=dataset.iloc[:,3:13].values
Y=dataset.iloc[:,13].values

#Encoding the categorical data
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
labelencoder_X_1=LabelEncoder()
X[:,1]=labelencoder_X_1.fit_transform(X[:,1])
labelencoder_X_2=LabelEncoder()
X[:,2]=labelencoder_X_2.fit_transform(X[:,2])
'''
here onehotencoder is applied to only 1 of X because to avoid dummy variable trap as it contain 
three categorical features and onehotencoder is not applied to 2 of X becoz it has only two 
categorical variables
'''

onehotencoder=OneHotEncoder(categorical_features=[1])
X=onehotencoder.fit_transform(X).toarray()
#removing one dummy variable
X=X[:,1:]

#Splitting the dataset into the training set and test set
from sklearn.model_selection import train_test_split
X_train,X_test,Y_train,Y_test=train_test_split(X,Y,test_size=0.2,random_state=0)

#Feature Scaling 
from sklearn.preprocessing import StandardScaler
sc=StandardScaler()
X_train=sc.fit_transform(X_train)
X_test=sc.fit_transform(X_test)

# ANN Making
# Importing the Keras libraries and packages
from keras.models import Sequential
from keras.layers import Dense

#Intializing the ANN
#we can intialize the by two ways either by defining the sequence of layers or by defining
#the graph 
classifier = Sequential()

#Adding the input layer and the first hidden layer
#kernel_intializer='uniform' sets the weight through small numbers and follow uniform distribution
#activation is activation function in relu corresponds to the rectifier function  
classifier.add(Dense(6,kernel_initializer='uniform',activation='relu',input_dim=11))

#Adding the second hidden layer
classifier.add(Dense(6,kernel_initializer='uniform',activation='relu'))

#Adding the ouput layer
classifier.add(Dense(1,kernel_initializer='uniform',activation='sigmoid'))

#Compiling the ANN
#optimizer is the algorithm which is used to optimize the weights,here we use the adams algo
#which is one of the stochastic gradient algorithm 
#loss is the fuction used to optimize in weights,this is used in matematics
#see video 22for more detail
#the accuracy criterian is used to impprove the weights after every batch so that the weights 
#can improve

classifier.compile(optimizer='adam',loss='binary_crossentropy',metrics=['accuracy'])

#batch_size is the size of rows taken at a time
classifier.fit(X_train,Y_train,batch_size=10,epochs=100)

#Making the predictions and evaluating the model

#Predicting the test results
Y_pred=classifier.predict(X_test)
Y_pred=(Y_pred>0.5)

#Making the confusion matrix
from sklearn.metrics import confusion_matrix
cm=confusion_matrix(Y_test,Y_pred)